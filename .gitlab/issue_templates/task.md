## Background

[//]: # (Provide a background or the root/source that justifies this task or action.)

## Task description

[//]: # (Short summary of the action to be executed)

* [ ] Action 1
* [ ] Action 2
* [ ] Action 3

## Acceptance Criteria

[//]: # (Acceptance criteria should follow the S.M.A.R.T. principle https://en.wikipedia.org/wiki/SMART_criteria )

----

/assign @toscalix
/milestone %"BuildStream_v1.1" %"BuildStream_v1.2"
/label ~Coordination ~Release ~Roadmap
/label ~Important ~Urgent ~Critical
